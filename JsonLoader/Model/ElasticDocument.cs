﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace JsonLoader.Model
{
    public class ElasticDocument
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public long Year { get; set; }

        public string Summary { get; set; }

        public string Body { get; set; }

        public string Geo1 { get; set; }

        public string SourceType { get; set; }

        public string Url { get; set; }

        public List<string> Places { get; set; }

        public List<string> People { get; set; }

        public List<string> Companies { get; set; }

        public List<string> BiGrams { get; set; }

        public List<string> TriGrams { get; set; }

        public List<string> SocialTags { get; set; }

        public List<string> Topics { get; set; }

        public List<string> Industry { get; set; }

        public List<string> Technology { get; set; }

        public long BodyWordCnt { get; set; }
    }
}
