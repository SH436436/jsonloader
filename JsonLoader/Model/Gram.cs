﻿namespace JsonLoader.Model
{
    public class Gram
    {
        public string Words { get; set; }
        public long Count { get; set; }
    }
}