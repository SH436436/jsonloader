﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonLoader.Model
{
    public class LoaderConfig
    {
        public string ElasticUrl { get; set; }
        public string IndexName { get; set; }
        public string TypeName { get; set; }
        public string FilePath { get; set; }
    }
}
