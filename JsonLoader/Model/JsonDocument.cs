﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace JsonLoader.Model
{
    public class JsonDocument
    {
        [JsonProperty("m_szDocID")]
        public string Id { get; set; }

        [JsonProperty("m_szDocTitle")]
        public string Title { get; set; }

        [JsonProperty("m_szYear")]
        public long Year { get; set; }

        [JsonProperty("m_szDocSumamry")]
        public string Summary { get; set; }

        [JsonProperty("m_szDocBody")]
        public string Body { get; set; }

        [JsonProperty("m_szGeo1")]
        public string Geo1 { get; set; }

        [JsonProperty("m_szSourceType")]
        public string SourceType { get; set; }

        [JsonProperty("m_szSrcUrl")]
        public string Url { get; set; }

        [JsonProperty("m_Places")]
        public string[] Places { get; set; }

        [JsonProperty("m_People")]
        public string[] People { get; set; }

        [JsonProperty("m_Companies")]
        public string[] Companies { get; set; }

        [JsonProperty("m_BiGrams")]
        public string[] BiGrams { get; set; }

        [JsonProperty("m_TriGrams")]
        public string[] TriGrams { get; set; }

        [JsonProperty("m_SocialTags")]
        public string[] SocialTags { get; set; }

        [JsonProperty("m_Topics")]
        public string[] Topics { get; set; }

        [JsonProperty("m_Industry")]
        public string[] Industry { get; set; }

        [JsonProperty("m_Technology")]
        public string[] Technology { get; set; }

        [JsonProperty("m_BiCnt")]
        public long[] BiCnt { get; set; }

        [JsonProperty("m_TriCnt")]
        public long[] TriCnt { get; set; }

        [JsonProperty("m_iDocBodyWordCnt")]
        public long DocBodyWordCnt { get; set; }
    }
}
