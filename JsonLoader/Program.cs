﻿using JsonLoader.Model;
using Microsoft.Extensions.Configuration;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace JsonLoader
{
    class LoadJson
    {
        public static void Main(string[] args)
        {
            var config = GetConfiguration();

            var settings = new ConnectionSettings(new Uri(config.ElasticUrl))
               .DefaultIndex(config.IndexName)
               .DefaultTypeName(config.TypeName);

            var client = new ElasticClient(settings);

            client.DeleteIndex(config.IndexName);

            var result = client.CreateIndex(config.IndexName, c => c
                .Mappings(ms => ms
                    .Map<ElasticDocument>(m => m
                        .AutoMap()
                    )
                )
            );

            if (!result.IsValid)
            {
                throw new Exception(result.DebugInformation);
            }

            //reset client with default settings
            client = new ElasticClient(settings);

            var documents = Directory.GetFiles(config.FilePath, "*.json").ToList();

            Parallel.ForEach(documents, (@document) =>
            {
                var serializer = new JsonSerializer();

                using (var file = File.OpenText(@document))
                {
                    var doc = (JsonDocument)serializer.Deserialize(file, typeof(JsonDocument));
                    var indexDoc = MapFromJson(doc);
                    var indexResponse = client.IndexDocument(indexDoc);
                    if (!result.IsValid)
                    {
                        throw new Exception(result.DebugInformation);
                    }
                }
            });
        }

        private static ElasticDocument MapFromJson(JsonDocument doc)
        {
            var document = new ElasticDocument
            {
                Id = doc.Id,
                Title = doc.Title,
                Year = doc.Year,
                Summary = doc.Summary,
                Body = doc.Body,
                Geo1 = doc.Geo1,
                SourceType = doc.SourceType.Replace("source_",""),
                Url = doc.Url,
                Places = doc.Places.ToList(),
                People = doc.People.ToList(),
                Companies = doc.Companies.ToList(),
                SocialTags = doc.SocialTags.ToList(),
                Topics = doc.Topics.ToList(),
                Industry = doc.Industry.ToList(),
                Technology = doc.Technology.ToList(),
                BiGrams = MapNGram(doc.BiGrams, doc.BiCnt).OrderByDescending(a=>a.Count).Select(a=>a.Words).ToList(),
                TriGrams = MapNGram(doc.TriGrams, doc.TriCnt).OrderByDescending(a=>a.Count).Select(a =>a.Words).ToList(),
                BodyWordCnt = doc.DocBodyWordCnt
            };
            return document;
        }

        private static List<Gram> MapNGram(string[] gram, long[] count)
        {
            var gramLength = gram.Length;
            var grams = new List<Gram>();
            if(gramLength == count.Length)
            {
                for (int i = 0; i< gramLength; i+=1)
                {
                    grams.Add(new Gram
                    {
                        Words = gram[i],
                        Count = count[i]
                    });
                }
            }
            return grams;
        }

        private static LoaderConfig GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();

            Console.WriteLine(configuration.GetConnectionString("Storage"));

            var config = configuration.GetSection("LoaderConfig").Get<LoaderConfig>();
            return config;
        }
    }
}
